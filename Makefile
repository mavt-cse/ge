BIN = $(HOME)/bin
B = $(BIN)
# [p]rograms
P = merge filter
install:; 
	mkdir -p "$B"
	for f in $P; do cp "$$f" "$B/ge.$$f"; done
.PHONY: install
